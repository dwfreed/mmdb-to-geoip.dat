import functools
import geoip_binary_search_tree
import ipaddress
import sys


# DB types
GEOIP_COUNTRY_EDITION = 1
GEOIP_CITY_EDITION_REV1 = 2
GEOIP_ASNUM_EDITION = 9
GEOIP_COUNTRY_EDITION_V6 = 12
GEOIP_CITY_EDITION_REV1_V6 = 30
GEOIP_ASNUM_EDITION_V6 = 21


# Misc constants
RECORD_SIZE = 3
NODE_SIZE = RECORD_SIZE * 2
STRUCTURE_INFO_MAX_SIZE = 20
DATABASE_INFO_MAX_SIZE = 100
STRUCTURE_INFO_DELIMITER = b'\xff\xff\xff'
DATABASE_INFO_DELIMITER = b'\x00\x00\x00'
COUNTRY_NODE_COUNT = 16776960
MAX_ASN_DATA_LENGTH = 300
MAX_CITY_DATA_LENGTH = 50


int_from_little_bytes = functools.partial(int.from_bytes, byteorder="little")
int_to_node_little_bytes = functools.partial(int.to_bytes, length=NODE_SIZE, byteorder="little")
int_to_record_little_bytes = functools.partial(int.to_bytes, length=RECORD_SIZE, byteorder="little")


class GeoLiteDatDB:
    def __init__(self, db_type, tree, node_count, data, info, struct_node_count=0, current_node_id=0):
        self.db_type = db_type
        self.tree = tree
        self.node_count = node_count
        self.data = data
        self.info = info
        self.struct_node_count = struct_node_count
        self.current_node_id = current_node_id

    def _serialize_record_data(self, record):
        if not record.data:
            return b'\x00'
        if self.db_type in (GEOIP_ASNUM_EDITION, GEOIP_ASNUM_EDITION_V6):
            if len(record.data) < MAX_ASN_DATA_LENGTH:
                return record.data + b'\x00'
            else:
                return record.data
        else:
            tmp = [bytes([record.data["country"]])]
            left = MAX_CITY_DATA_LENGTH - 1
            for field in ("region", "city", "postal_code"):
                if left:
                    left -= len(record.data[field])
                    if left:
                        tmp.append(record.data[field] + b'\x00')
                        left -= 1
                    else:
                        tmp.append(record.data[field])
            for field in ("latitude", "longitude"):
                if left >= RECORD_SIZE:
                    tmp.append(int_to_record_little_bytes(record.data[field]))
                    left -= RECORD_SIZE
            if record.data["country"] == 225: # US
                if left >= RECORD_SIZE:
                    tmp.append(int_to_record_little_bytes(record.data["metroarea"]))
            return b''.join(tmp)

    def dump(self, filename):
        with open(filename, 'wb') as f:
            node_id = 0
            for node in self.tree:
                if node.index is None:
                    node.index = node_id
                    node_id += 1

            if self.db_type not in (GEOIP_COUNTRY_EDITION, GEOIP_COUNTRY_EDITION_V6):
                if node_id:
                    offset = node_id
                else:
                    offset = self.node_count
                serialized_data = []
                for node in self.data:
                    node.index = offset
                    serialized = self._serialize_record_data(node)
                    serialized_data.append(serialized)
                    offset += len(serialized)

            node_id = 0
            for node in self.tree:
                if node.index == node_id:
                    node_bytes = int_to_node_little_bytes(node.right.index * (2**8)**RECORD_SIZE + node.left.index)
                    f.write(node_bytes)
                    node_id += 1

            print("node_count:", self.node_count)
            print("struct_node_count:", self.struct_node_count)
            print("current_node_id:", self.current_node_id)
            print("nodes:", node_id)

            if self.db_type not in (GEOIP_COUNTRY_EDITION, GEOIP_COUNTRY_EDITION_V6):
                f.write(b''.join(serialized_data))

            if self.info:
                f.write(DATABASE_INFO_DELIMITER)
                f.write(self.info)

            if self.db_type != GEOIP_COUNTRY_EDITION:
                f.write(STRUCTURE_INFO_DELIMITER)
                f.write(self.db_type.to_bytes(1, "little"))
                f.write(self.struct_node_count.to_bytes(RECORD_SIZE, "little"))


class GeoLiteDatParser:
    def __init__(self, filename):
        self.filename = filename

    def _load_record_data(self, offset):
        if self.db_type in (GEOIP_CITY_EDITION_REV1, GEOIP_CITY_EDITION_REV1_V6):
            result = {}
            result["country"] = self.file_data[offset]
            offset += 1
            left = MAX_CITY_DATA_LENGTH - 1
            for field in ("region", "city", "postal_code"):
                if left > 0:
                    end = self.file_data.find(b'\x00', offset, offset + left)
                    if end == -1:
                        end = offset + left
                    result[field] = self.file_data[offset:end]
                    length = end - offset
                    if left > length:
                        length += 1
                    offset += length
                    left -= length
            for field in ("latitude", "longitude"):
                if left >= RECORD_SIZE:
                    result[field] = int_from_little_bytes(self.file_data[offset:offset+RECORD_SIZE])
                    offset += RECORD_SIZE
                    left -= RECORD_SIZE
            if result["country"] == 225: # US
                if left >= RECORD_SIZE:
                    result["metroarea"] = int_from_little_bytes(self.file_data[offset:offset+RECORD_SIZE])
            return geoip_binary_search_tree.CityRecordData(result)
        elif self.db_type in (GEOIP_ASNUM_EDITION, GEOIP_ASNUM_EDITION_V6):
            end = self.file_data.find(b'\x00', offset, offset + MAX_ASN_DATA_LENGTH)
            if end == -1:
                end = offset + MAX_ASN_DATA_LENGTH
            return geoip_binary_search_tree.ASNRecordData(self.file_data[offset:end])
        else:
            return geoip_binary_search_tree.CountryRecordData(offset)

    def _print_path(self, path, target):
        netmask = len(path)
        if self.db_type in (GEOIP_COUNTRY_EDITION, GEOIP_ASNUM_EDITION, GEOIP_CITY_EDITION_REV1):
            intaddr = int(path + '0' * (32 - netmask), 2)
            addr = ipaddress.IPv4Network((intaddr, netmask))
        else:
            intaddr = int(path + '0' * (128 - netmask), 2)
            addr = ipaddress.IPv6Network((intaddr, netmask))
        if target == self.node_count:
            target = 'missing'
        else:
            target = 'present'
        print("%s: %s" % (str(addr), target))

    def _recursive_parse(self, node, offset, path):
        right, left = divmod(int_from_little_bytes(self.file_data[offset:offset+NODE_SIZE]), 2**24)

        if left < self.node_count:
            if left not in self.nodes:
                self.current_node_id += 1
                new_node = geoip_binary_search_tree.TreeNode(self.current_node_id)
                self.nodes[left] = new_node
            else:
                new_node = self.nodes[left]
            node.left = new_node
            self._recursive_parse(new_node, left*NODE_SIZE, path + '0')
        else:
            self._print_path(path + '0', left)
            if self.db_type not in (GEOIP_COUNTRY_EDITION, GEOIP_COUNTRY_EDITION_V6):
                left += (NODE_SIZE - 1) * self.node_count
            if left in self.offset_to_data_index:
                node.left = self.data[self.offset_to_data_index[left]]
            else:
                node.left = self._load_record_data(left)
                self.data.append(node.left)
                self.offset_to_data_index[left] = len(self.data) - 1

        if right < self.node_count:
            if right not in self.nodes:
                self.current_node_id += 1
                new_node = geoip_binary_search_tree.TreeNode(self.current_node_id)
                self.nodes[right] = new_node
            else:
                new_node = self.nodes[right]
            node.right = new_node
            self._recursive_parse(new_node, right*NODE_SIZE, path + '1')
        else:
            self._print_path(path + '1', right)
            if self.db_type not in (GEOIP_COUNTRY_EDITION, GEOIP_COUNTRY_EDITION_V6):
                right += (NODE_SIZE - 1) * self.node_count
            if right in self.offset_to_data_index:
                node.right = self.data[self.offset_to_data_index[right]]
            else:
                node.right = self._load_record_data(right)
                self.data.append(node.right)
                self.offset_to_data_index[right] = len(self.data) - 1

    def parse(self):
        self.db_type = GEOIP_COUNTRY_EDITION
        self.node_count = COUNTRY_NODE_COUNT
        self.struct_node_count = 0
        self.info = None

        with open(self.filename, 'rb') as f:
            self.file_data = bytearray(f.read())

        offset = self.file_data.rfind(STRUCTURE_INFO_DELIMITER, -STRUCTURE_INFO_MAX_SIZE - len(STRUCTURE_INFO_DELIMITER))
        if offset > -1:
            cursor = offset + len(STRUCTURE_INFO_DELIMITER)
            self.db_type = self.file_data[cursor]
            cursor += 1
            self.struct_node_count = int_from_little_bytes(self.file_data[cursor:cursor+RECORD_SIZE])
            if self.db_type != GEOIP_COUNTRY_EDITION_V6:
                self.node_count = self.struct_node_count
            del self.file_data[offset:]

        offset = self.file_data.rfind(DATABASE_INFO_DELIMITER, -DATABASE_INFO_MAX_SIZE - len(DATABASE_INFO_DELIMITER))
        if offset > -1:
            cursor = offset + len(DATABASE_INFO_DELIMITER)
            self.info = bytes(self.file_data[cursor:])
            del self.file_data[offset:]

        self.tree = geoip_binary_search_tree.TreeNode(0)
        self.nodes = {0: self.tree}
        self.current_node_id = 0
        self.data = []
        self.offset_to_data_index = {}
        offset = self.node_count * NODE_SIZE
        if self.db_type in (GEOIP_CITY_EDITION_REV1, GEOIP_CITY_EDITION_REV1_V6):
            self.data.append(geoip_binary_search_tree.CityRecordData())
        elif self.db_type in (GEOIP_ASNUM_EDITION, GEOIP_ASNUM_EDITION_V6):
            self.data.append(geoip_binary_search_tree.ASNRecordData())
        else:
            self.data.append(geoip_binary_search_tree.CountryRecordData(COUNTRY_NODE_COUNT))
            offset = self.node_count
        self.offset_to_data_index[offset] = len(self.data) - 1

        self._recursive_parse(self.tree, 0, '')

        return GeoLiteDatDB(self.db_type, self.tree, self.node_count, self.data, self.info, self.struct_node_count, self.current_node_id)


if __name__ == "__main__":
    parser = GeoLiteDatParser(sys.argv[1])
    db = parser.parse()
    db.dump(sys.argv[2])
