class TreeNode:
    __slots__ = ('left', 'right', 'index')

    def __init__(self, index=None):
        self.left = None
        self.right = None
        self.index = index

    def __iter__(self):
        yield self
        if isinstance(self.left, TreeNode):
            yield from self.left
        if isinstance(self.right, TreeNode):
            yield from self.right


class RecordData:
    __slots__ = ('index',)

    def __init__(self):
        self.index = None


class ASNRecordData(RecordData):
    __slots__ = ('data',)

    def __init__(self, data=b''):
        super().__init__()
        self.data = data

class CityRecordData(RecordData):
    __slots__ = ('data',)

    def __init__(self, data=None):
        super().__init__()
        self.data = data

class CountryRecordData(RecordData):
    __slots__ = ()

    def __init__(self, index):
        super().__init__()
        self.index = index
